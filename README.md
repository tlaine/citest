# Continous integration

[![pipeline status](https://gitlab.inria.fr/tlaine/citest/badges/master/pipeline.svg)](https://gitlab.inria.fr/tlaine/citest/commits/master)
[![Quality Gate](https://sonarqube.inria.fr/sonarqube/api/badges/gate?key=thibaultlaine:citest)](https://sonarqube.inria.fr/sonarqube/dashboard/index/thibaultlaine:citest)
[![Coverage](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=thibaultlaine:citest&metric=coverage)](https://sonarqube.inria.fr/sonarqube/dashboard/index/thibaultlaine:citest)

[![Bugs](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=thibaultlaine:citest&metric=bugs)](https://sonarqube.inria.fr/sonarqube/dashboard/index/thibaultlaine:citest)
[![Vulnerabilities](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=thibaultlaine:citest&metric=vulnerabilities)](https://sonarqube.inria.fr/sonarqube/dashboard/index/thibaultlaine:citest)
[![Code smells](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=thibaultlaine:citest&metric=code_smells)](https://sonarqube.inria.fr/sonarqube/dashboard/index/thibaultlaine:citest)

[![Line of code](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=thibaultlaine:citest&metric=ncloc)](https://sonarqube.inria.fr/sonarqube/dashboard/index/thibaultlaine:citest)
[![Comment ratio](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=thibaultlaine:citest&metric=comment_lines_density)](https://sonarqube.inria.fr/sonarqube/dashboard/index/thibaultlaine:citest)

# Links
- Sonarqube : https://sonarqube.inria.fr/sonarqube/dashboard?id=thibaultlaine%3Acitest
- Socumentation : https://tlaine.gitlabpages.inria.fr/citest/

# Install & dependancies
need the rosbridge_suite package to run,

```bash
sudo apt-get install ros-melodic-rosbridge-server
```

# Informations
Simple test of CI for an hello world project in ROS
Implement also the ROS <> Unity bridge (just the ROS part, the unity one is located at https://gitlab.inria.fr/tlaine/citestunity)

# Quick launch
start the launch file cilauncher.launch :

```bash
roslaunsh cilauncher.launch
```
