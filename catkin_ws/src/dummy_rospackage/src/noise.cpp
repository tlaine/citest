/** @file noise.cpp
 *  \brief Noise ROS node.
 *         Chaotic attractor noise generator.
 *
 *  The node implement a 3D Lorenz attractor.
 *  The result is published in two messages : 
 *  - "point_stream" aka ros::Publisher<geometry_msgs::PointStamped> : the actual point position
 *  - "trajectory_stream" aka ros::Publisher<visualization_msgs::Marker> : the trajectory of the last 1000 point in time
 *
 *  \todo
 *    - need to add message suscribing for node control : 
 *      - trajectory color, thickness, ...
 *      - trajectory point queue size
 *      - attractor speed, and parameters
 *    - change dt dependant to loop rate
 */

//	includes
#include "ros/ros.h"
#include "std_msgs/String.h"
#include <geometry_msgs/PointStamped.h>
#include <visualization_msgs/Marker.h>
#include "dummy_rospackage/SetSpeed.h"

#include <sstream>
#include <iostream>
#include <algorithm>

//	constants
#define MESSAGE_QUEUE 10
#define LOOP_RATE 60
#define TRAJECTORY_QUEUE 1000
#define TRAJECTORY_THICKNESS 0.1f
#define TRAJECTORY_COLOR_R 0.f
#define TRAJECTORY_COLOR_G 1.f
#define TRAJECTORY_COLOR_B 0.f
#define TRAJECTORY_COLOR_A 1.f

float speed = 10.f;

bool SetSpeed(dummy_rospackage::SetSpeed::Request  &req, dummy_rospackage::SetSpeed::Response &res)
{
    speed = std::min(10.f, std::max(req.data, 0.1f));
    res.data = speed;
    return true;
}


// node process
int main(int argc, char **argv)
{
    // initialize node and message topics
    ros::init(argc, argv, "lorenzAttractor");
    ros::NodeHandle n;
    ros::Publisher point_pub = n.advertise<geometry_msgs::PointStamped>("point_stream", MESSAGE_QUEUE);
    ros::Publisher trajectory_pub = n.advertise<visualization_msgs::Marker>("trajectory_stream", MESSAGE_QUEUE);
    ros::ServiceServer service = n.advertiseService("noise_speed", SetSpeed);
    ros::Rate loop_rate(LOOP_RATE);

    // innitialize trajectory message
    visualization_msgs::Marker line_strip;
    line_strip.header.frame_id = "map";
    line_strip.header.stamp = ros::Time();
    line_strip.ns = "my_namespace";
    line_strip.id = 0;
    line_strip.type = visualization_msgs::Marker::LINE_STRIP;
    line_strip.action = visualization_msgs::Marker::ADD;
    line_strip.pose.orientation.x = 0.0;
    line_strip.pose.orientation.y = 0.0;
    line_strip.pose.orientation.z = 0.0;
    line_strip.pose.orientation.w = 1.0;
    line_strip.scale.x = TRAJECTORY_THICKNESS;
    line_strip.scale.y = TRAJECTORY_THICKNESS;
    line_strip.scale.z = TRAJECTORY_THICKNESS;
    line_strip.color.a = TRAJECTORY_COLOR_A;
    line_strip.color.r = TRAJECTORY_COLOR_R;
    line_strip.color.g = TRAJECTORY_COLOR_G;
    line_strip.color.b = TRAJECTORY_COLOR_B;

    // initial point position
    float x = 0.0f;
    float y = 0.5f;
    float z = 1.1f;

    // chaotic attractor constants
    const float a = 10;
    const float b = 28;
    const float c = 8.f/3.f;
    const float dt = 0.0016f;

    while (ros::ok())
    {
        // computing speed
        float dx = a * (y - x);
        float dy = b * x - y - x * z;
        float dz = x * y - c * z;

        // integration
        x += speed * dt * dx;
        y += speed * dt * dy;
        z += speed * dt * dz;

        // computing point message
        geometry_msgs::PointStamped ps;
        ps.point.x = x;
        ps.point.y = y;
        ps.point.z = z;
        ps.header.frame_id = "map";
        ps.header.stamp = ros::Time::now();

        // computing trajectory message
        line_strip.points.push_back(ps.point);
        while (line_strip.points.size() > TRAJECTORY_QUEUE)
        {
            line_strip.points.erase(line_strip.points.begin());
        }	
        line_strip.header.stamp = ros::Time::now();

        // debug and end loop
        std_msgs::String msg;
        std::stringstream ss;
        ss << '[' << x << ' ' << y << ' ' << z << "] ; [" << dx << ' ' << dy << ' ' << dz << ']';
        msg.data = ss.str();
        std::cout << msg.data << std::endl;
        point_pub.publish(ps);
        trajectory_pub.publish(line_strip);
        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}

